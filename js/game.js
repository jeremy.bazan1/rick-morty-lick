// calcule taille dispo pour cont_log

let sort_size = document.querySelector("#cont_sort").offsetHeight;
let game_size = document.querySelector("#cont_game").offsetHeight;
let section_size = document.querySelector("section").offsetHeight;
let size = sort_size + game_size;
let dispo_size = section_size - size;

//prise longueur/largeur cont_game pour page_pres

let game_width = document.querySelector("#cont_game").offsetWidth;
let game_height = document.querySelector("#cont_game").offsetHeight;

window.onload = function () {
    document.querySelector("#page_pres").style.height = game_height + "px";
    document.querySelector("#page_pres").style.width = game_width + "px";
    document.querySelector("#cont_log").style.height = dispo_size + "px";

}

//changer les tailles quand la taille de l'ecran change

window.onresize = function () {
    let sort_size = document.querySelector("#cont_sort").offsetHeight;
    let game_size = document.querySelector("#cont_game").offsetHeight;
    let section_size = document.querySelector("section").offsetHeight;
    let game_width = document.querySelector("#cont_game").offsetWidth;
    let game_height = document.querySelector("#cont_game").offsetHeight;

    document.querySelector("#page_pres").style.height = game_height + "px";
    document.querySelector("#page_pres").style.width = game_width + "px";
    document.querySelector("#cont_log").style.height = dispo_size + "px";
};

//remove page_pres au click

//variable verif debut game
let verif = false;

document.querySelector("#remove").addEventListener("click", function () {
    document.querySelector("#page_pres").remove();
    selection();
});

//CREATION JOUEUR

let player = new Charactere("morty", 200, "avatar");

let lifePlayer = player.life;

document.querySelector("#progressPlayer").value = lifePlayer;

//CREATION ORDI
let computer = new Charactere("pickleRick", 200, "avatar");

let computerLife = computer.life;

document.querySelector("#progressAI").value = computerLife;

//CREATION SORT JOUEUR

let sort_a = new Spell("attack", 15, "show me what you got", Infinity);
let sort_b = new Spell("slash", 25, "Existence is pain!", 5);
let sort_c = new Spell("strick", 35, "Nobody exists on purpose. Nobody belongs anywhere. We're all going to die. Come watch TV.", 2);
let sort_d = new Spell("heal", 50, "love me like you do", 4);

//CREATION SORT ORDI
let skill_a = new Spell("earth", 15, "I'm Pickle Riiiiick", Infinity);
skill_a.animate = "./css/Earth4.png";
skill_a.time = 5;
skill_a.keyName = "earth";
skill_a.sound = "./audio/Earth9.m4a";
let skill_b = new Spell("ice", 25, "It's time to get schwifty in here", 5);
skill_b.animate = "./css/Ice4.png";
skill_b.time = 4;
skill_b.keyName = "ice";
skill_b.sound = "./audio/Ice7.m4a";
let skill_c = new Spell("thunder", 35, "wubalubadubdub", 2);
skill_c.animate = "./css/Thunder3.png";
skill_c.time = 5;
skill_c.keyName = "thunder";
skill_c.sound = "./audio/Thunder11.m4a";
let skill_d = new Spell("heal", 50, "Weddings are basically funerals with cake.", 4);
skill_d.animate = "./css/HitPhoton.png";
skill_d.time = 4;
skill_d.keyName = "heal";
skill_d.sound = "./audio/Saint7.m4a";


//CREATION VARIABLE LIMIT SORT
let sortRestantA = sort_a.limitSort;
let sortRestantB = sort_b.limitSort;
let sortRestantC = sort_c.limitSort;
let sortRestantD = sort_d.limitSort;

//AFFICHAGE STATS/NOM SORTS 
document.querySelector("#life_player").textContent = lifePlayer;
document.querySelector("#name_player").textContent = player.name;
document.querySelector("#life_computer").textContent = computerLife;
document.querySelector("#name_computer").textContent = computer.name;
document.querySelector("#nameA").textContent = sort_a.name;
document.querySelector("#nameB").textContent = sort_b.name;
document.querySelector("#nameC").textContent = sort_c.name;
document.querySelector("#nameD").textContent = sort_d.name;
document.querySelector("#limitA").textContent = "\u221e/\u221e";
document.querySelector("#limitB").textContent = sortRestantB + "/" + sort_b.limitSort;
document.querySelector("#limitC").textContent = sortRestantC + "/" + sort_c.limitSort;
document.querySelector("#limitD").textContent = sortRestantD + "/" + sort_d.limitSort;


//TIMER ENTRE CLIQUE
let timer = true;

function time() {
    timer = false;
    setTimeout(function () {
        timer = true;
    }, 1200);
}

//ADDEVENT CLICK SORT

document.querySelector("#sort_a").addEventListener("click", function () {
    if (verif === true) {
        if (timer === true) {
            if (sortRestantA > 0) {
                sortRestantA--;
                sound_attack("./audio/Sword6.m4a");
                let degat = new playerDamage(computerLife, sort_a.damage, sort_a.sentence);
                degat.funcAttack();
                let anim = new Animation("./css/Hit1.png", 4, "player", "hitA");
                anim.animation();
                setTimeout(function () {
                    let random = new randomAttack(skill_a, skill_b, skill_c, skill_d);
                    random.randomAttack();
                    winLose();
                }, 1000);
                time();

            }
            else {
                let p = new Psentence("Sort non disponible");
                p.Psentence();
            }
        }
    }
});

document.querySelector("#sort_b").addEventListener("click", function () {
    if (verif === true) {
        if (sortRestantB > 0) {
            if (timer === true) {
                sortRestantB--;
                sound_attack("./audio/Slash13.m4a");
                document.querySelector("#limitB").textContent = sortRestantB + "/" + sort_b.limitSort;
                let degat = new playerDamage(computerLife, sort_b.damage, sort_b.sentence);
                degat.funcAttack();
                let anim = new Animation("./css/Slash.png", 5, "player", "slash");
                anim.animation();
                setTimeout(function () {
                    let random = new randomAttack(skill_a, skill_b, skill_c, skill_d);
                    random.randomAttack();
                    winLose();
                }, 1000);
                time();
            }
            else {
                let p = new Psentence("Sort non disponible");
                p.Psentence();
            }
        }
    }
});

document.querySelector("#sort_c").addEventListener("click", function () {
    if (verif === true) {
        if (timer === true) {
            if (sortRestantC > 0) {
                sortRestantC--;
                sound_attack("./audio/Explosion3.m4a");
                document.querySelector("#limitC").textContent = sortRestantC + "/" + sort_c.limitSort;
                let degat = new playerDamage(computerLife, sort_c.damage, sort_c.sentence);
                degat.funcAttack();
                let anim = new Animation("./css/Hit2.png", 3, "player", "hitB");
                anim.animation();
                setTimeout(function () {
                    let random = new randomAttack(skill_a, skill_b, skill_c, skill_d);
                    random.randomAttack();
                    winLose();
                }, 1000);
                time();
            }
            else {
                let p = new Psentence("Sort non disponible");
                p.Psentence();
            }
        }
    }
});

document.querySelector("#sort_d").addEventListener("click", function () {
    if (verif === true) {
        if (timer === true) {
            if (sortRestantD > 0) {
                sortRestantD--;
                sound_attack("./audio/Saint7.m4a");
                document.querySelector("#limitD").textContent = sortRestantD + "/" + sort_d.limitSort;
                let heal = new Heal(lifePlayer, sort_d.damage, player.life, player);
                heal.funcHeal();
                let p = new Psentence("Player: " + sort_d.sentence);
                p.Psentence();
                document.querySelector("#life_player").textContent = lifePlayer;
                document.querySelector("#progressPlayer").value = lifePlayer;
                let anim = new Animation("./css/HitPhoton.png", 5, "computer", "heal");
                anim.animation();
                setTimeout(function () {
                    let random = new randomAttack(skill_a, skill_b, skill_c, skill_d);
                    random.randomAttack();
                    winLose();
                }, 1000);
                time();
            }
            else {
                let p = new Psentence("Sort non disponible");
                p.Psentence();
            }
        }
    }
});


