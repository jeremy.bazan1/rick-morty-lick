class Heal {
    constructor(life, heal, maxLife, type) {
        this.life = life;
        this.heal = heal;
        this.maxLife = maxLife;
        this.type = type;
    }

    funcHeal() {
        let lifeHeal = this.life + this.heal;

        if(lifeHeal > this.maxLife) {
            lifeHeal = this.maxLife;
        }

        if(this.type === player) {
            lifePlayer = lifeHeal;
        }

        else if(this.type === computer) {
            computerLife = lifeHeal;
        }
    }
}