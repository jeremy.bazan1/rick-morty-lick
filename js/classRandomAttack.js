class randomAttack {
    constructor(skill_a, skill_b, skill_c, skill_d) {
        this.skill_a = skill_a;
        this.skill_b = skill_b;
        this.skill_c = skill_c;
        this.skill_d = skill_d;
    }

    randomAttack() {
        let random = Math.floor(Math.random() * 4);
        let choice = [this.skill_a, this.skill_b, this.skill_c, this.skill_d];
        let attack = choice[random];
        if (random === 3) {
            if(attack.limitSort > 0) {
                attack.limitSort--;
                sound_attack(attack.sound);
            let heal = new Heal(computerLife, attack.damage, computer.life, computer);
            heal.funcHeal();
            let p = new Psentence("Ordi: " + attack.sentence);
            p.Psentence(); 
            let anim = new Animation(attack.animate, attack.time, "player", attack.keyName);
            anim.animation();
            document.querySelector("#life_computer").textContent = computerLife;
            document.querySelector("#progressAI").value = computerLife;
            }
            else {
                let relance = new randomAttack(skill_a, skill_b, skill_c, skill_d);
                relance.randomAttack();
            }
        }
        else {
            if(attack.limitSort > 0) {
                attack.limitSort--;
                sound_attack(attack.sound);
            let anim = new Animation(attack.animate, attack.time, "computer", attack.keyName);
            anim.animation();
            let degat = new AIdamage(lifePlayer, attack.damage, attack.sentence);
            degat.funcAttackIA();
            }
            else {
                let relance = new randomAttack(skill_a, skill_b, skill_c, skill_d);
                relance.randomAttack();
            }
        }
    }
}