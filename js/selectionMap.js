function selection() {
    let select_width = document.querySelector("#cont_game").offsetWidth;
    let select_height = document.querySelector("#cont_game").offsetHeight;
    let div = document.createElement("div");
    let divImg = document.createElement("div");
    let texte  = document.createElement("p");
    let imgA = document.createElement("div");
    let imgB = document.createElement("div");
    let imgC = document.createElement("div");
    let imgD = document.createElement("div");
    div.style.height = select_height + "px";
    div.style.width = select_width + "px";
    texte.textContent = "choose your map";
    texte.style.fontSize = 34 + "px";
    imgA.style.backgroundImage = "url(./css/backgroundGame1.png)";
    imgB.style.backgroundImage = "url(./css/backgroundGame2.jpg)";
    imgC.style.backgroundImage = "url(./css/backgroundGame3.jpg)";
    imgD.style.backgroundImage = "url(./css/backgroundGame4.png)";
    div.classList.add("page_pres", "d-flex", "flex-column");
    texte.classList.add("d-flex", "m-auto");
    divImg.classList.add("d-flex", "justify-content-around", "mb-auto" , "flex-row");
    imgA.classList.add("imgSelect");
    imgB.classList.add("imgSelect");
    imgC.classList.add("imgSelect");
    imgD.classList.add("imgSelect");
    document.querySelector("#cont_game").appendChild(div);
    div.appendChild(texte);
    div.appendChild(divImg);
    divImg.appendChild(imgA);
    divImg.appendChild(imgB);
    divImg.appendChild(imgC);
    divImg.appendChild(imgD);
    audio_select();
    imgA.addEventListener("click", function() {
        verif = true;
        document.querySelector("#cont_game").style.backgroundImage = imgA.style.backgroundImage;
        div.remove();
        audio_play();
    });
    imgB.addEventListener("click", function() {
        verif = true;
        document.querySelector("#cont_game").style.backgroundImage = imgB.style.backgroundImage;
        div.remove();
        audio_play();
    });
    imgC.addEventListener("click", function() {
        verif = true;
        document.querySelector("#cont_game").style.backgroundImage = imgC.style.backgroundImage;
        div.remove();
        audio_play();
    });
    imgD.addEventListener("click", function() {
        verif = true;
        document.querySelector("#cont_game").style.backgroundImage = imgD.style.backgroundImage;
        div.remove();
        audio_play();
    });
}