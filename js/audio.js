let audio_slt = new Audio("./audio/go_son_go.m4a");
let audio_attack = new Audio("");

document.querySelector("#audio").addEventListener("click", function() {
    if(audio_slt.muted === true) {
        audio_slt.muted = false;
        audio_attack.muted = false;
        let icone = document.querySelector("#mute");
        icone.classList.remove("fa", "fa-volume-off");
        icone.classList.add("fa", "fa-volume-up");
    }
    else {
    audio_slt.muted = true;
    audio_attack.muted = true;
    let icone = document.querySelector("#mute");
    icone.classList.remove("fa", "fa-volume-up");
    icone.classList.add("fa", "fa-volume-off");
    }
});

function audio_select() {
    audio_slt.play();
}

function audio_play() {
    audio_slt.src = "./audio/stealth_infiltration.m4a";
    audio_slt.play();
    audio_slt.loop = true;
}

function audio_win() {
    audio_slt.loop = false;
    audio_slt.src = "./audio/blips_and_chitz.m4a";
    audio_slt.play();
}

function audio_lose() {
    audio_slt.loop = false;
    audio_slt.src = "./audio/a_ton_of_bad_memories.m4a";
    audio_slt.play();
}

function sound_attack(file) {
    audio_attack.src = file;
    audio_attack.loop = false;
    audio_attack.play();
}